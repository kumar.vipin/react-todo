import React from 'react';


class Cart extends React.Component {

    _renderItems() {
        return this.props.items.map((item, index) =>
            (
                <tr key={index}>
                    <td>
                        {item.name}
                    </td>
                    <td>
                        {item.qty}
                    </td>
                    <td>
                        ${item.price * item.qty}
                    </td>
                    <td>
                        <button className="btn btn-danger btn-xs">Remove</button>
                    </td>
                </tr>
            )
        )
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <table className="table table-bordered">
                        <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Qty
                            </th>
                            <th>
                                Price
                            </th>
                            <th>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {this._renderItems()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Cart;