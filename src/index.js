import React from 'react';
import ReactDOM from 'react-dom';

import Header from './Header';
import Products from './Products';
import Cart from './Cart';

class App extends React.Component {
    constructor() {
        super(...arguments);
        this.state = {
            products: [
                {
                    id: 1,
                    name: 'IPhone 7',
                    price: 300,
                    image: 'https://store.storeimages.cdn-apple.com/4974/as-images.apple.com/is/image/AppleInc/aos/published/images/H/KH/HKHC2/HKHC2?wid=445&hei=445&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=1474481298618'
                },
                {
                    id: 2,
                    name: 'Samsung S8',
                    price: 400,
                    image: 'https://staticshop.o2.co.uk/product/images/samsung_galaxy_s8_64gb_midnight_black_front_sku_header.png?cb=b1e52dd9252967784a9580c9de7ea7e3'
                },
                {
                    id: 3,
                    name: 'Moto X',
                    price: 100,
                    image: 'http://cdn.mos.cms.futurecdn.net/58312c7950492b496f5ae5f02f80582c-480-80.jpg'
                },
                {
                    id: 4,
                    name: 'Xiomi',
                    price: 50,
                    image: 'http://global.mifile.cn/webfile/globalimg/en/hd/2014082501/redmi/new_xmhm_14.jpg?2014021402'
                }
            ],
            cart: [],
            component: 'products'
        }
    }

    addToCartHandler(product) {
        const existedItemIndex = this.state.cart.findIndex((item) => {
            return item.id === product.id;
        });

        if (existedItemIndex > -1) {
            const cart = [...this.state.cart];
            cart[existedItemIndex].qty += 1;
            this.setState({
                cart
            });
        } else {
            this.setState({
                cart: [...this.state.cart, {...product, qty: 1}]
            });
        }
    }

    _changeComponent(component) {
        this.setState({
            component
        })
    }

    _renderComponent() {
        if(this.state.component === 'products') {
            return (
                <Products
                    onAddToCart={this.addToCartHandler.bind(this)}
                    products={this.state.products}
                />
            )
        } else {
            return  <Cart items={this.state.cart}/>
        }
    }

    render() {
        return (
            <div className="container">
                <Header onLinkClick={this._changeComponent.bind(this)} cart={this.state.cart}/>
                {this._renderComponent()}
            </div>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById('root'));
