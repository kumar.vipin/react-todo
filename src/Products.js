import React from 'react';
import Product from './Product';


class Products extends React.Component {

    _renderProducts() {
        return this.props.products.map(product =>
            (
                <Product
                    onAddToCart={event => this.props.onAddToCart(product)}
                    product={product}
                    key={product.id}
                />
            )
        )
    }

    render() {
        return (
            <div className="row">
                {this._renderProducts()}
            </div>
        );
    }
}

export default Products;