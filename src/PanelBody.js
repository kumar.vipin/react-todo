import React from 'react';

class PanelBody extends React.Component {
    render() {
        const {children} = this.props;
        return (
            <div className="panel-body">
                {children}
            </div>

        );
    }
}

export default PanelBody;