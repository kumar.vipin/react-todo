import React from 'react';

class Header extends React.Component {

    render() {
        const {cart, onLinkClick} = this.props;
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">E Store</a>
                    </div>
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                            <li><a onClick={e => onLinkClick('products')} href="#">Products</a></li>
                            <li><a onClick={e => onLinkClick('cart')} href="#">Cart ({cart.length})</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
};

export default Header;