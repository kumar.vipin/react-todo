import React from 'react';

import Col from './Col';
import Panel from './Panel';
import PanelHeading from './PanelHeading';
import PanelBody from './PanelBody';

class Product extends React.Component {


    render() {
        const {product, onAddToCart} = this.props;
        return (
            <Col part="3">
                <Panel>
                    <PanelHeading>
                        {product.name}
                    </PanelHeading>
                    <PanelBody>
                        <div className="col-md-12">
                            <div className="pull-left">
                                <img style={{height: '70px', width: '70px'}}
                                     src={product.image}
                                     alt={product.name}
                                />

                            </div>
                            <div className="pull-left" style={{width: '70px', fontSize: '10px', paddingLeft: '10px  '}}>
                                <div>
                                    Price: ${product.price}
                                </div>
                                <br/>
                                <div>
                                    <button onClick={onAddToCart} className="btn btn-primary btn-xs">Add To Cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    </PanelBody>
                </Panel>
            </Col>
        );
    }
}

export default Product;